#pragma once

#include <GL/gl3w.h>

#include "../eyes/Shader.h"
#include "../eyes/Texture.h"

#include <map>

class ResourceManager
{
  public:
    static ResourceManager &instance();

    Shader &loadShader(const std::string &v_shader_file, const std::string &f_Shader_file, const std::string &id);
    Shader &getShader(const std::string &id);

    Texture2D &loadTexture(const std::string &file, const std::string &id);

    Texture2D &getTexture(const std::string &id);

    void clear();

  private:
    ResourceManager();

    static ResourceManager *m_instance;

    std::map<std::string, Shader> m_shaders;
    std::map<std::string, Texture2D> m_textures;
};