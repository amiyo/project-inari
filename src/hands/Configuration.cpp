#include "Configuration.h"

#include "FileSystem.h"

#include <algorithm>
#include <cctype>
#include <fstream>

Configuration::Configuration(const std::string &file, const std::string &default)
    : m_file(file)
{
    if (std::ifstream(file).good())
    {
        readFile();
    }
    else
    {
        std::ofstream(file) << default;
        readFile();
    }
}

void Configuration::setValue(const std::string &section, const std::string &key, const Value &value)
{
    m_settings[section][key] = value;
    saveFile();
}

Value Configuration::getValue(const std::string &section, const std::string &key)
{
    auto it_section = m_settings.find(section);
    if (it_section != m_settings.end())
    {
        auto it_key = it_section->second.find(key);
        if (it_key != it_section->second.end())
        {
            return it_key->second;
        }
    }

    return Value();
}

void Configuration::readFile()
{
    std::string section;
    FileSystem::instance().readFile(m_file, [this, &section](std::string line) {
        line = line.substr(0, line.find(";"));
        if (!line.empty())
        {
            line.erase(std::remove_if(line.begin(), line.end(), std::isspace), line.end());
            auto equal_pos = line.find("=");
            auto brackets_pos = line.find("[");

            if (equal_pos == std::string::npos && brackets_pos != std::string::npos)
            {
                auto open_bracket = brackets_pos + 1;
                section = line.substr(open_bracket, line.find_last_of(']') - open_bracket);
            }
            else if (equal_pos != std::string::npos)
            {
                std::string parameter = line.substr(0, equal_pos);
                m_settings[section][parameter] = Value(line.substr(equal_pos + 1));
            }
        }
    });
}

void Configuration::saveFile()
{
    std::string contents;
    for (const auto &section : m_settings)
    {
        contents += '[' + section.first + "]\n";

        for (const auto &value : section.second)
        {
            contents += value.first + '=' + value.second.toString() + '\n';
        }

        contents += '\n';
    }
    FileSystem::instance().writeFile(m_file, contents, std::ofstream::out | std::ofstream::trunc);
}