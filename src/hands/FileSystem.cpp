#include "FileSystem.h"

FileSystem *FileSystem::m_instance = nullptr;

FileSystem &FileSystem::instance()
{
    if (!m_instance)
    {
#ifdef _WINDOWS
        m_instance = new WinFileSystem();
#endif
    }
    return *m_instance;
}