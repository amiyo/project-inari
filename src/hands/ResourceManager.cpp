#include "ResourceManager.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

ResourceManager &ResourceManager::instance()
{
    if (!m_instance)
    {
        m_instance = new ResourceManager();
    }
    return *m_instance;
}

Shader &ResourceManager::loadShader(const std::string &v_shader_file, const std::string &f_shader_file,
                                    const std::string &id)
{
    m_shaders[id] = Shader(v_shader_file, f_shader_file);
    return m_shaders[id];
}

Shader &ResourceManager::getShader(const std::string &id)
{
    return m_shaders[id];
}

Texture2D &ResourceManager::loadTexture(const std::string &file, const std::string &id)
{
    Texture2D texture;

    int width = 0;
    int height = 0;
    int nrchannels = 0;

    auto image = stbi_load(file.c_str(), &width, &height, &nrchannels, 0);

    if (nrchannels > 3)
    {
        texture.setInternalFormat(GL_RGBA);
        texture.setImageFormat(GL_RGBA);
    }

    texture.generate(width, height, image);
    stbi_image_free(image);

    m_textures[id] = texture;
    return m_textures[id];
}

Texture2D &ResourceManager::getTexture(const std::string &id)
{
    return m_textures[id];
}

void ResourceManager::clear()
{
    for (auto shader : m_shaders)
    {
        glDeleteProgram(shader.second.getId());
    }

    for (auto texture : m_textures)
    {
        auto id = texture.second.getID();
        glDeleteTextures(1, &id);
    }
}

ResourceManager::ResourceManager()
{
}

ResourceManager *ResourceManager::m_instance = nullptr;