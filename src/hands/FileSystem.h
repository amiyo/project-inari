#pragma once

#include <fstream>
#include <functional>
#include <string>

class FileSystem
{
  public:
    static FileSystem &instance();

    virtual ~FileSystem()
    {
    }
    virtual std::string readFile(
        const std::string &path,
        const std::function<void(const std::string &)> &reading =
            [](auto line) {
            },
        const std::ifstream::openmode &mode = std::ifstream::in)
    {
        std::string result;
        std::ifstream file(path, mode);

        if (file.is_open())
        {
            std::string line;
            while (std::getline(file, line))
            {
                result += line + '\n';
                reading(line);
            }
        }

        file.close();
        return result;
    }

    virtual bool writeFile(const std::string &path, const std::string &contents,
                           const std::ofstream::openmode &mode = std::ofstream::out)
    {
        bool result = false;
        std::ofstream file(path, mode);

        if (file.is_open())
        {
            file.write(contents.c_str(), contents.size());
            result = true;
        }

        file.close();
        return result;
    }

    virtual std::string getBinPath(const std::string &append = "") = 0;

  protected:
    FileSystem()
    {
    }

  private:
    static FileSystem *m_instance;
};

#ifdef _WINDOWS
#include <windows.h>

#include <pathcch.h>
#pragma comment(lib, "Pathcch.lib")

class WinFileSystem : public FileSystem
{
  public:
    virtual std::string getBinPath(const std::string &append = "") override
    {
        // Get path to executable file
        char *bin_file;
        size_t buf;

        _get_pgmptr(&bin_file);
        std::wstring wbin_file(strlen(bin_file) + 1, L'#');
        mbstowcs_s(&buf, &wbin_file[0], wbin_file.size(), bin_file, strlen(bin_file));
        PathCchRemoveFileSpec(&wbin_file[0], wbin_file.size());

        // Append path
        if (!append.empty())
        {
            std::wstring wappend(append.size() + 1, L'#');
            mbstowcs_s(&buf, &wappend[0], wappend.size(), append.c_str(), append.size());
            PathCchAppend(&wbin_file[0], wbin_file.size(), &wappend[0]);
        }

        // Convert to string
        wcstombs_s(&buf, bin_file, wbin_file.size() - 1, &wbin_file[0], wbin_file.size());
        return bin_file;
    }
};
#endif