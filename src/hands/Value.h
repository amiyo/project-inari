#pragma once

#include <string>

class Value
{
  public:
    Value();

    Value(const std::string &value);
    Value(bool value);
    Value(int value);
    Value(float value);

    std::string toString() const;
    bool toBool();
    int toInt();
    float toFloat();

  private:
    std::string m_value;

    bool isNumber();
};