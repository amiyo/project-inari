#pragma once

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include "editor/Editor.h"
#include "hands/Configuration.h"
#include "hands/Logger.hpp"

#include <memory>

class InariEngine
{
  public:
    InariEngine(Configuration &config, Logger &logger);

    void run();

  private:
    GLFWwindow *m_window;
    Configuration &m_config;
    Logger &m_logger;
    Editor m_editor;

    void initWindow();
    void mainLoop();
    void cleanUp();
};
