#include "InariEngine.h"

#include "hands/ResourceManager.h"
#include "hands/Value.h"

#include <functional>
#include <iostream>

InariEngine::InariEngine(Configuration &config, Logger &logger)
    : m_config(config)
    , m_logger(logger)
{
}

void InariEngine::run()
{
    m_logger.debug("%s()", __FUNCTION__);
    initWindow();
    mainLoop();
    cleanUp();
}

void InariEngine::initWindow()
{
    m_logger.debug("%s()", __FUNCTION__);
    glfwSetErrorCallback([](int error, const char *description) {
        std::cerr << "GLFW error [" << error << "]: " << description << std::endl;
    });

    if (!glfwInit())
    {
        throw std::runtime_error("Failed to initialize GLFW!");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    std::string title = "Inari Engine";
#ifdef VERSION
    title += " v." + std::string(VERSION);
#endif

    auto window_width = m_config.getValue("Video", "Width").toInt();
    auto window_height = m_config.getValue("Video", "Height").toInt();

    if (m_config.getValue("Video", "Fullscreen").toBool())
    {
        const auto *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

        m_window = glfwCreateWindow(mode->width, mode->height, title.c_str(), glfwGetPrimaryMonitor(), nullptr);
    }
    else
    {
        m_window = glfwCreateWindow(window_width, window_height, title.c_str(), nullptr, nullptr);
    }

    if (!m_window)
    {
        throw std::runtime_error("Failed to create window!");
    }

    glfwMakeContextCurrent(m_window);
    glfwSwapInterval(1); // NOTE VSync: 0 = Disable, 1 = Enable, 2 and more =
                         // Lock on division of default frame rate

    if (gl3wInit())
    {
        throw std::runtime_error("Failed to initialize OpenGL!");
    }

    if (!gl3wIsSupported(3, 3))
    {
        throw std::runtime_error("OpenGL 3.3 not supported!");
    }

    glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow *window, int width, int height) {
        glViewport(0, 0, width, height);
    });
    m_editor.configure(m_window);
    m_logger.setLogCallback(std::bind(&Editor::addLog, &m_editor, std::placeholders::_1));
}

void InariEngine::mainLoop()
{
#ifdef VERSION
    m_logger.comment("Inari %s Awaken", VERSION);
#else
    m_logger.comment("Inari Awaken");
#endif
    m_logger.debug("%s()", __FUNCTION__);
    while (!glfwWindowShouldClose(m_window))
    {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        glfwPollEvents();

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        m_editor.draw();

        glfwMakeContextCurrent(m_window);
        glfwSwapBuffers(m_window);
    }
}

void InariEngine::cleanUp()
{
    m_logger.debug("%s()", __FUNCTION__);
    m_editor.clean();
    ResourceManager::instance().clear();
    glfwDestroyWindow(m_window);
    glfwTerminate();
}