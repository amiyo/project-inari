#include "shader.h"

#include <fstream>
#include <iostream>
#include <vector>

Shader::Shader()
    : m_id(0)
{
}

Shader::Shader(const std::string &vertex_path, const std::string &fragment_path)
    : m_id(0)
{
    compile(vertex_path, fragment_path);
}

void Shader::compile(const std::string &vertex_path, const std::string &fragment_path)
{
    try
    {
        std::string vertex_code;
        std::string fragment_code;

        if (!readFileToString(vertex_path, vertex_code) & !readFileToString(fragment_path, fragment_code))
        {
            throw std::runtime_error("Trouble with reading shader code");
        }

        auto vert_shader_code = vertex_code.c_str();
        auto frag_shader_code = fragment_code.c_str();

        auto vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &vert_shader_code, nullptr);
        glCompileShader(vertex);
        if (!checkCompileError(vertex, CompileType::VERTEX))
        {
            throw std::runtime_error("Trouble with compiling vertex shader");
        }

        auto fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &frag_shader_code, nullptr);
        glCompileShader(fragment);
        if (!checkCompileError(fragment, CompileType::FRAGMENT))
        {
            throw std::runtime_error("Trouble with compiling fragment shader");
        }

        m_id = glCreateProgram();
        glAttachShader(m_id, vertex);
        glAttachShader(m_id, fragment);
        glLinkProgram(m_id);
        if (!checkCompileError(m_id, CompileType::PROGRAM))
        {
            throw std::runtime_error("Trouble with linking shader program");
        }

        glDeleteShader(vertex);
        glDeleteShader(fragment);
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << "[ERROR] Shader::init | " << e.what() << std::endl;
    }
}

void Shader::use() const
{
    if (m_id != 0)
    {
        glUseProgram(m_id);
    }
}

Shader &Shader::setFloat(const std::string &name, GLfloat value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform1f(glGetUniformLocation(m_id, name.c_str()), value);

    return *this;
}

Shader &Shader::setInteger(const std::string &name, GLint value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform1i(glGetUniformLocation(m_id, name.c_str()), value);

    return *this;
}

Shader &Shader::setVector2f(const std::string &name, GLfloat x, GLfloat y, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform2f(glGetUniformLocation(m_id, name.c_str()), x, y);

    return *this;
}

Shader &Shader::setVector2f(const std::string &name, const glm::vec2 &value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform2f(glGetUniformLocation(m_id, name.c_str()), value.x, value.y);

    return *this;
}

Shader &Shader::setVector3f(const std::string &name, GLfloat x, GLfloat y, GLfloat z, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform3f(glGetUniformLocation(m_id, name.c_str()), x, y, z);

    return *this;
}

Shader &Shader::setVector3f(const std::string &name, const glm::vec3 &value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform3f(glGetUniformLocation(m_id, name.c_str()), value.x, value.y, value.z);

    return *this;
}

Shader &Shader::setVector4f(const std::string &name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform4f(glGetUniformLocation(m_id, name.c_str()), x, y, z, w);

    return *this;
}

Shader &Shader::setVector4f(const std::string &name, const glm::vec4 &value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniform4f(glGetUniformLocation(m_id, name.c_str()), value.x, value.y, value.z, value.w);

    return *this;
}

Shader &Shader::setMatrix4(const std::string &name, const glm::mat4 &value, GLboolean use_shader)
{
    if (use_shader)
    {
        use();
    }
    glUniformMatrix4fv(glGetUniformLocation(m_id, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));

    return *this;
}

GLuint Shader::getId()
{
    return m_id;
}

bool Shader::checkCompileError(const GLuint &shader, const CompileType &type)
{
    int success = 0;
    int log_length = 0;
    std::vector<char> info_log;

    switch (type)
    {
    case CompileType::VERTEX:
    case CompileType::FRAGMENT:
    {
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            info_log.resize(log_length + 1);
            glGetShaderInfoLog(shader, log_length, nullptr, info_log.data());
            std::cerr << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << info_log.data() << "\n";
            return false;
        }

        break;
    }
    case CompileType::PROGRAM:
    {
        glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &log_length);
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success)
        {
            info_log.resize(log_length + 1);
            glGetProgramInfoLog(shader, log_length, nullptr, info_log.data());
            std::cerr << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << info_log.data() << "\n";
            return false;
        }

        break;
    }
    }

    return true;
}

bool Shader::readFileToString(const std::string &path, std::string &output)
{
    try
    {
        std::ifstream file;
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        file.open(path);

        output.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

        file.close();
    }
    catch (const std::ios_base::failure &e)
    {
        std::cerr << "[ERROR] Shader::Shader: Fail file reading: " << e.what() << std::endl;
        return false;
    }
    return true;
}
