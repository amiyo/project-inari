#include "Texture.h"

Texture2D::Texture2D()
    : m_width(0)
    , m_height(0)
    , m_internal_format(GL_RGB)
    , m_image_format(GL_RGB)
    , m_wrap_s(GL_REPEAT)
    , m_wrap_t(GL_REPEAT)
    , m_filter_min(GL_LINEAR)
    , m_filter_max(GL_LINEAR)
{
    glGenTextures(1, &m_id);
}

void Texture2D::generate(GLuint width, GLuint height, unsigned char *data)
{
    m_width = width;
    m_height = height;

    bind();
    glTexImage2D(GL_TEXTURE_2D, 0, m_internal_format, width, height, 0, m_image_format, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_filter_min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_filter_max);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::bind() const
{
    glBindTexture(GL_TEXTURE_2D, m_id);
}

GLuint Texture2D::getID() const
{
    return m_id;
}

GLuint Texture2D::getWidth() const
{
    return m_width;
}

GLuint Texture2D::getHeight() const
{
    return m_height;
}

Texture2D &Texture2D::setInternalFormat(GLuint internal_format)
{
    m_internal_format = internal_format;
    return *this;
}

Texture2D &Texture2D::setImageFormat(GLuint image_format)
{
    m_image_format = image_format;
    return *this;
}

Texture2D &Texture2D::setWrapS(GLuint wrap_s)
{
    m_wrap_s = wrap_s;
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_s);
    glBindTexture(GL_TEXTURE_2D, 0);
    return *this;
}

Texture2D &Texture2D::setWrapT(GLuint wrap_t)
{
    m_wrap_t = wrap_t;
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_t);
    glBindTexture(GL_TEXTURE_2D, 0);
    return *this;
}

Texture2D &Texture2D::setFilterMin(GLuint filter_min)
{
    m_filter_min = filter_min;
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_min);
    glBindTexture(GL_TEXTURE_2D, 0);
    return *this;
}

Texture2D &Texture2D::setFilterMax(GLuint filter_max)
{
    m_filter_max = filter_max;
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_max);
    glBindTexture(GL_TEXTURE_2D, 0);
    return *this;
}
