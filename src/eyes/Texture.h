#pragma once

#include <GL/gl3w.h>

#include <vector>

class Texture2D
{
  public:
    Texture2D();

    // Generates texture from image data
    void generate(GLuint width, GLuint height, unsigned char *data);
    // Binds the texture as the current active GL_TEXTURE_2D texture object
    void bind() const;

    GLuint getID() const;

    GLuint getWidth() const;
    GLuint getHeight() const;

    Texture2D &setInternalFormat(GLuint internal_format);
    Texture2D &setImageFormat(GLuint image_format);

    Texture2D &setWrapS(GLuint wrap_s);
    Texture2D &setWrapT(GLuint wrap_t);
    Texture2D &setFilterMin(GLuint filter_min);
    Texture2D &setFilterMax(GLuint filter_max);

  private:
    GLuint m_id;

    GLuint m_width;
    GLuint m_height;

    GLuint m_internal_format;
    GLuint m_image_format;

    GLuint m_wrap_s;
    GLuint m_wrap_t;
    GLuint m_filter_min;
    GLuint m_filter_max;
};