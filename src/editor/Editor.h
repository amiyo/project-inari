#pragma once

#include <GLFW/glfw3.h>

#include <string>

#include "imgui/imgui.h"

class Editor
{
  public:
    Editor();

    void configure(GLFWwindow *window);
    void draw();
    void clean();

    void addLog(const std::string &message);

  private:
    int drawMainMenuBar();
    void drawLogger(const ImVec2 &size, const ImVec2 &pos);
    void drawHierarchy(const ImVec2 &size, const ImVec2 &pos);
    void drawProperties(const ImVec2 &size, const ImVec2 &pos);
    void drawPlayground(const ImVec2 &size, const ImVec2 &pos);
    void drawFileSystem(const ImVec2 &size, const ImVec2 &pos);

    GLFWwindow *m_window;

    // Logger
    ImGuiTextBuffer m_log_buffer;
    bool m_log_scroll_to_bottom;
    bool m_log_wrap;
    int m_log_count;
};