# Inari Engine

> Mystic gates to the world of imagination and fairy tales opened by O-Inari Sama

## Description

Role-playing game creation toolbox.

## Roadmap

* 1.0 - Phoenix state - Mix of classical game engines like as Infinity Engine (Baldur's Gate, Planescape: Torment, Icewind Dale), Fallout Engine, Square Engine (Final Fantasy, Chrono Trigger, Secret of Mana);
* 2.0 - Fox state - Modernization;
* 3.0 - Okami state - Three-dimensional Engine.

## Requirements

| Technology                                               | Description                                      |
| -------------------------------------------------------- | ------------------------------------------------ |
| **[C++](https://isocpp.org/)**                           | Core language                                    |
| **[OpenGL](https://opengl.org/)**                        | Render SDK                                       |
| **[CMake](https://cmake.org/)**                          | Build system                                     |
| **[GLFW](https://www.glfw.org/)**                        | Window handler                                   |
| **[gl3w](https://github.com/skaslev/gl3w)**              | GL loader                                        |
| **[glm](https://glm.g-truc.net/) or OpenGL Mathematics** | C++ mathematics library for graphics programming |
| **[stb-image](https://github.com/nothings/stb)**         | Image processing library                         |

---

Amiyo Hisamov 2018-2020
